package com.fewbits.postwatcher.domain.events;

public interface EventBus {

    void postEvent(final Object event);
    void register(final Object event);
    void unregister(final Object event);

}
