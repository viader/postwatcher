package com.fewbits.postwatcher.controllers.rest.endpoint;

import retrofit.Endpoint;

public interface DynamicEndpoint extends Endpoint {
    void setObject(Object object);
}