package com.fewbits.postwatcher.controllers.rest;

public interface RestManager {

    void rebuildAdapter(String username);

    void getPosts(int startPosition, int limit);
}