package com.fewbits.postwatcher.model.repository;


import com.fewbits.postwatcher.controllers.rest.model.PostJson;

import java.util.Collection;

public interface PostEntityRepository
{
    void savePost(Collection<PostJson> post);
}
