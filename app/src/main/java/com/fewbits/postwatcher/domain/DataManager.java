package com.fewbits.postwatcher.domain;

import com.fewbits.postwatcher.domain.repository.PostRepository;

public interface DataManager {
    PostRepository getPostRepository();


}
