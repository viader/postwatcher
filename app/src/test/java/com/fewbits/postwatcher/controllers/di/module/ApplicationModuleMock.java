package com.fewbits.postwatcher.controllers.di.module;


import com.fewbits.postwatcher.controllers.MyApplication;
import com.fewbits.postwatcher.controllers.rest.ConnectivityManager;
import com.fewbits.postwatcher.controllers.rest.ConnectivityManagerMock;
import com.fewbits.postwatcher.controllers.rest.endpoint.DynamicEndpoint;
import com.fewbits.postwatcher.controllers.rest.endpoint.TumblrEndpointMock;

public class ApplicationModuleMock extends ApplicationModule
{

    public ApplicationModuleMock(MyApplication pApplication)
    {
        super(pApplication);
    }

    @Override
    protected ConnectivityManager providesConnectivityManager()
    {
        return new ConnectivityManagerMock();
    }

    @Override
    protected DynamicEndpoint providesDynamicEndpoint()
    {
        return new TumblrEndpointMock();
    }
}
