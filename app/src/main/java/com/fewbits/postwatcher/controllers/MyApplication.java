package com.fewbits.postwatcher.controllers;

import android.app.Application;
import android.content.Intent;

import com.fewbits.postwatcher.controllers.di.component.ApplicationComponent;
import com.fewbits.postwatcher.controllers.di.component.DaggerApplicationComponent;
import com.fewbits.postwatcher.controllers.di.module.ApplicationModule;
import com.fewbits.postwatcher.controllers.service.SampleDataLoadService;

public class MyApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate()
    {
        super.onCreate();
        init();
    }

    protected void init()
    {
        initializeInjector(getApplicationModule());
    }

    protected void initializeInjector(ApplicationModule applicationModule)
    {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(applicationModule)
                .build();
    }

    protected void initializeDataService()
    {
        startService(new Intent(this, SampleDataLoadService.class));
    }

    public ApplicationComponent getApplicationComponent()
    {
        return applicationComponent;
    }

    public ApplicationModule getApplicationModule()
    {
        return new ApplicationModule(this);
    }


}
