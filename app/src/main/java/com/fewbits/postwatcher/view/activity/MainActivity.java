package com.fewbits.postwatcher.view.activity;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.fewbits.postwatcher.R;
import com.fewbits.postwatcher.domain.Post;
import com.fewbits.postwatcher.view.activity.base.BaseActivity;
import com.fewbits.postwatcher.view.adapter.PostAdapter;
import com.fewbits.postwatcher.view.presenter.MainActivityPresenter;
import com.fewbits.postwatcher.view.presenter.MainActivityPresenterImpl;
import com.fewbits.postwatcher.view.view.MainActivityView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainActivityView
{

    @Bind(R.id.activity_main_recycler_view)
    RecyclerView recyclerView;

    private MainActivityPresenter presenter;
    private List<Post> posts = new ArrayList<>();
    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initData();
        initUserInterface();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater lInflater = getMenuInflater();
        lInflater.inflate(R.menu.activity_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) this.getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView;

        if(searchItem != null)
        {
            searchView = (SearchView) searchItem.getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
            {
                @Override
                public boolean onQueryTextSubmit(String query)
                {
                    return presenter.onQueryTextSubmit(query);
                }

                @Override
                public boolean onQueryTextChange(String query)
                {
                    return presenter.onQueryTextChange(query);
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }

    public void initData() {
        presenter = new MainActivityPresenterImpl(this, getApplicationComponent());
        adapter = new PostAdapter(this, posts);
    }

    public void initUserInterface() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    public void setItems(List<Post> items) {
        posts.clear();
        posts.addAll(items);
        adapter.notifyDataSetChanged();
    }
}
