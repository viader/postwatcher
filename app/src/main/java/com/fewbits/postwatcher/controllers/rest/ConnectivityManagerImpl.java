package com.fewbits.postwatcher.controllers.rest;

import android.net.NetworkInfo;


public class ConnectivityManagerImpl implements ConnectivityManager
{

    private android.net.ConnectivityManager connectivityManager;

    public ConnectivityManagerImpl(android.net.ConnectivityManager connectivityManager)
    {
        this.connectivityManager = connectivityManager;
    }

    public boolean isConnectionAvailable()
    {
        NetworkInfo lNetInfo = connectivityManager.getActiveNetworkInfo();
        return lNetInfo != null && lNetInfo.isConnected();
    }
}
