package com.fewbits.postwatcher.model.repository.tumblr;

import com.fewbits.postwatcher.controllers.rest.model.PostsResponseJson;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class DeserializeJsonTest
{

    public static Gson gson;

    static
    {
        gson = new GsonBuilder().create();
    }

    @Test
    public void shouldSuccessDeserialize()
    {
        ClassLoader classLoader = getClass().getClassLoader();
        PostsResponseJson response = null;
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(classLoader.getResource("postsResponse.json").getPath()));
            response = gson.fromJson(reader, PostsResponseJson.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(response.getPostsStart()).isEqualTo(0);
        assertThat(response.getPostsTotal()).isEqualTo(131);
        assertThat(response.getPosts().size()).isEqualTo(20);
    }

}
