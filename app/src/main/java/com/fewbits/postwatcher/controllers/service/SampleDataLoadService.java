package com.fewbits.postwatcher.controllers.service;

import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.fewbits.postwatcher.domain.Post;
import com.fewbits.postwatcher.domain.repository.PostRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SampleDataLoadService extends BaseService {

    public static final int CYCLICAL_REFRESH_TIME = 1000;
    private int counter = 0;
    @Inject
    PostRepository postRepository;
    @Inject
    Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();
        getApplicationComponent().inject(this);
        loadSampleData();
    }

    private void loadSampleData() {
        try {
            InputStream stream = getAssets().open("posts.json");
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
            final List<Post> posts = gson.fromJson(reader, new TypeToken<ArrayList<Post>>() {}.getType());
            postRepository.savePosts(posts);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Post post = new Post();
                    post.setTitle("" + counter++);
                    postRepository.savePost(post);
                    handler.postDelayed(this, CYCLICAL_REFRESH_TIME);
                }
            }, CYCLICAL_REFRESH_TIME);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
