package com.fewbits.postwatcher.view.activity.base;

import android.support.v7.app.AppCompatActivity;

import com.fewbits.postwatcher.controllers.MyApplication;
import com.fewbits.postwatcher.controllers.di.component.ApplicationComponent;
import com.fewbits.postwatcher.controllers.di.component.ApplicationComponentProvider;

public class BaseActivity extends AppCompatActivity implements ApplicationComponentProvider {
    public ApplicationComponent getApplicationComponent()
    {
        return ((MyApplication) getApplication()).getApplicationComponent();
    }
}
