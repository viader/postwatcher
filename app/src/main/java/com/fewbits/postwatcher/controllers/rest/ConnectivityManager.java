package com.fewbits.postwatcher.controllers.rest;

public interface ConnectivityManager
{
    boolean isConnectionAvailable();
}
