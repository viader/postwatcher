package com.fewbits.postwatcher.controllers.rest.endpoint;

public class TumblrEndpoint implements DynamicEndpoint {

    private String username;

    public TumblrEndpoint(String username) {
        this.username = username;
    }

    public void setObject(Object object) {
        this.username = (String) object;
    }

    @Override
    public String getUrl() {
        return "http://"+username+".tumblr.com/";
    }

    @Override
    public String getName() {
        return "tumblr";
    }
}