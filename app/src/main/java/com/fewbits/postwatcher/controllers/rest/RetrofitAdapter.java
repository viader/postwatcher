package com.fewbits.postwatcher.controllers.rest;

import com.fewbits.postwatcher.controllers.rest.model.PostsResponseJson;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface RetrofitAdapter {
    @GET("/api/read?json&debug=1")
    void getPosts(@Query("start") int startPosition, @Query("num") int limit, Callback<PostsResponseJson> cb);
}
