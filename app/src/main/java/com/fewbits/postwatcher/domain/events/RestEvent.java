package com.fewbits.postwatcher.domain.events;

public class RestEvent extends AbstractEvent {

    public enum Type
    {
        FAIL
    }

    public RestEvent(Type type)
    {
        super(type);
    }
}
