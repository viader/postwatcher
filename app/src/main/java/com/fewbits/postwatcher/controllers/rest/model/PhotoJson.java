package com.fewbits.postwatcher.controllers.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoJson
{
    @SerializedName("caption")
    private String caption;
    @SerializedName("photo-url-250")
    private String photoLowQuality;
    @SerializedName("photo-url-500")
    private String photoGoodQuality;
    @SerializedName("width")
    private String width;
    @SerializedName("height")
    private String height;

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getPhotoLowQuality() {
        return photoLowQuality;
    }

    public void setPhotoLowQuality(String photoLowQuality) {
        this.photoLowQuality = photoLowQuality;
    }

    public String getPhotoGoodQuality() {
        return photoGoodQuality;
    }

    public void setPhotoGoodQuality(String photoGoodQuality) {
        this.photoGoodQuality = photoGoodQuality;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "PhotoJson{" +
                "caption='" + caption + '\'' +
                ", photoLowQuality='" + photoLowQuality + '\'' +
                ", photoGoodQuality='" + photoGoodQuality + '\'' +
                ", width='" + width + '\'' +
                ", height='" + height + '\'' +
                '}';
    }
}