package com.fewbits.postwatcher.domain;


import com.fewbits.postwatcher.domain.repository.PostRepository;

public class DataManagerImpl implements DataManager {

    PostRepository postRepository;

    @Override
    public PostRepository getPostRepository() {
        return postRepository;
    }

    public void setPostRepository(PostRepository postRepository) {
        this.postRepository = postRepository;
    }
}
