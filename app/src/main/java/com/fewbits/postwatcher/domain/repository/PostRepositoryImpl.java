package com.fewbits.postwatcher.domain.repository;

import com.fewbits.postwatcher.domain.Post;
import com.fewbits.postwatcher.domain.events.EventCreator;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class PostRepositoryImpl implements PostRepository {

    List<Post> posts = new ArrayList<>();

    @Inject
    EventCreator eventCreator;

    public PostRepositoryImpl(EventCreator eventCreator) {
        this.eventCreator = eventCreator;
    }

    @Override
    public void savePosts(List<Post> posts) {
        this.posts.addAll(posts);
        eventCreator.createNewPostsEvent();
    }

    @Override
    public void clearPosts() {
        posts.clear();
    }

    @Override
    public void savePost(Post post) {
        this.posts.add(post);
        eventCreator.createNewPostsEvent();
    }

    @Override
    public List<Post> getAllPosts() {
        return posts;
    }
}
