package com.fewbits.postwatcher.controllers;

import com.fewbits.postwatcher.controllers.di.module.ApplicationModule;

public class ApplicationMock extends MyApplication
{
    @Override
    protected void init()
    {
        initializeInjector(getApplicationModule());
    }

    public void setApplicationModule(ApplicationModule pApplicationModule)
    {
        initializeInjector(pApplicationModule);
    }
}
