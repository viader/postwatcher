package com.fewbits.postwatcher.controllers.di.component;

import com.fewbits.postwatcher.controllers.di.module.ApplicationModule;
import com.fewbits.postwatcher.controllers.rest.RestManager;
import com.fewbits.postwatcher.controllers.service.SampleDataLoadService;
import com.fewbits.postwatcher.domain.DataManager;
import com.fewbits.postwatcher.domain.events.EventBus;
import com.fewbits.postwatcher.domain.events.EventCreator;
import com.fewbits.postwatcher.view.presenter.MainActivityPresenterImpl;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(MainActivityPresenterImpl presenter);
    void inject(SampleDataLoadService service);

    RestManager getRestManager();
    EventCreator getEventCreator();
    DataManager getDataManager();
    EventBus getEventBus();
}
