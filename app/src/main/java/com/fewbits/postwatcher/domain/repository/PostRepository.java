package com.fewbits.postwatcher.domain.repository;

import com.fewbits.postwatcher.domain.Post;

import java.util.List;

public interface PostRepository {

    void savePosts(List<Post> posts);

    void clearPosts();

    void savePost(Post post);

    List<Post> getAllPosts();

}
