package com.fewbits.postwatcher.domain.events;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;

public class EventBusImpl extends Bus implements EventBus {

    private final Handler mainThread = new Handler(Looper.getMainLooper());

    public EventBusImpl() {
    }

    @Override
    public void postEvent(final Object event) {
        if(Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        } else {
            this.mainThread.post(new Runnable() {
                public void run() {
                    EventBusImpl.this.post(event);
                }
            });
        }
    }
}
