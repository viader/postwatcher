package com.fewbits.postwatcher.controllers.rest.mapper;

import com.fewbits.postwatcher.controllers.rest.model.PostJson;
import com.fewbits.postwatcher.domain.Post;
import com.mobandme.android.transformer.Transformer;

public class PostJsonMapper {
    private static Transformer transformer;

    static {
        transformer = new Transformer.Builder().build(PostJson.class);
    }

    public static Post mapToPost(PostJson postJson) {
        return (Post) transformer.transform(postJson);
    }

}
