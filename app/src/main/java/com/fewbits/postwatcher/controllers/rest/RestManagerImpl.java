package com.fewbits.postwatcher.controllers.rest;

import com.fewbits.postwatcher.controllers.rest.endpoint.DynamicEndpoint;
import com.fewbits.postwatcher.controllers.rest.mapper.PostJsonMapper;
import com.fewbits.postwatcher.controllers.rest.model.PostJson;
import com.fewbits.postwatcher.controllers.rest.model.PostsResponseJson;
import com.fewbits.postwatcher.domain.DataManager;
import com.fewbits.postwatcher.domain.Post;
import com.fewbits.postwatcher.domain.events.EventCreator;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

public class RestManagerImpl implements RestManager {

    private ConnectivityManager connectivityManager;
    private EventCreator eventCreator;
    private DataManager dataManager;
    private Gson gson;

    private RetrofitAdapter adapter;
    private DynamicEndpoint endpoint;
    private ExecutorService executorService;

    public RestManagerImpl(DynamicEndpoint endpoint, ConnectivityManager connectivityManager, DataManager dataManager, EventCreator eventCreator, Gson gson) {
        this.endpoint = endpoint;
        this.connectivityManager = connectivityManager;
        this.dataManager = dataManager;
        this.eventCreator = eventCreator;
        this.gson = gson;

        executorService = Executors.newCachedThreadPool();
        buildAdapter();
    }

    @Override
    public void rebuildAdapter(String username) {
        endpoint.setObject(username);
        buildAdapter();
    }

    public void buildAdapter() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(endpoint)
                .setClient(new OkClient())
                .setExecutors(executorService, executorService)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        adapter = restAdapter.create(RetrofitAdapter.class);
    }

    @Override
    public void getPosts(int startPosition, int limit) {
        if(connectivityManager.isConnectionAvailable())
        {
            adapter.getPosts(startPosition, limit, new Callback<PostsResponseJson>() {
                @Override
                public void success(PostsResponseJson postsResponseJson, Response response) {
                    List<PostJson> postsToMap = postsResponseJson.getPosts();
                    List<Post> posts = new ArrayList<>();
                    for(int i = 0; i<postsToMap.size(); i++) {
                        posts.add(PostJsonMapper.mapToPost(postsToMap.get(i)));
                    }
                    dataManager.getPostRepository().savePosts(posts);
                }

                @Override
                public void failure(RetrofitError error) {
                    eventCreator.createRequestFailEvent();
                }
            });
        }
        else {
            eventCreator.createRequestFailEvent();
        }
    }
}