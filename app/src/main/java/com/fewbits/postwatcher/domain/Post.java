package com.fewbits.postwatcher.domain;

public class Post {

    private String title;
    private String photoLowQuality;
    private String photoGoodQuality;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhotoLowQuality() {
        return photoLowQuality;
    }

    public void setPhotoLowQuality(String photoLowQuality) {
        this.photoLowQuality = photoLowQuality;
    }

    public String getPhotoGoodQuality() {
        return photoGoodQuality;
    }

    public void setPhotoGoodQuality(String photoGoodQuality) {
        this.photoGoodQuality = photoGoodQuality;
    }
}