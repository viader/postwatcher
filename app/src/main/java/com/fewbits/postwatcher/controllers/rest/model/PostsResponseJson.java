package com.fewbits.postwatcher.controllers.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PostsResponseJson
{
    @SerializedName("posts-type")
    private String postsType;

    @SerializedName("posts-start")
    private int postsStart;

    @SerializedName("posts-total")
    private int postsTotal;

    @SerializedName("posts")
    private List<PostJson> posts;

    @SerializedName("tumblelog")
    private TumblelogJson tumblelog;

    public String getPostsType() {
        return postsType;
    }

    public void setPostsType(String postsType) {
        this.postsType = postsType;
    }

    public int getPostsStart() {
        return postsStart;
    }

    public void setPostsStart(int postsStart) {
        this.postsStart = postsStart;
    }

    public int getPostsTotal() {
        return postsTotal;
    }

    public void setPostsTotal(int postsTotal) {
        this.postsTotal = postsTotal;
    }

    public List<PostJson> getPosts() {
        return posts;
    }

    public void setPosts(List<PostJson> posts) {
        this.posts = posts;
    }

    public TumblelogJson getTumblelog() {
        return tumblelog;
    }

    public void setTumblelog(TumblelogJson tumblelog) {
        this.tumblelog = tumblelog;
    }

    @Override
    public String toString() {
        return "PostsResponseJson{" +
                "postsType='" + postsType + '\'' +
                ", postsStart=" + postsStart +
                ", postsTotal=" + postsTotal +
                ", posts=" + posts +
                ", tumblelog=" + tumblelog +
                '}';
    }
}