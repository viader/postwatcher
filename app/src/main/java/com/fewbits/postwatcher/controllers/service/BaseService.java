package com.fewbits.postwatcher.controllers.service;

import android.app.Service;

import com.fewbits.postwatcher.controllers.MyApplication;
import com.fewbits.postwatcher.controllers.di.component.ApplicationComponent;
import com.fewbits.postwatcher.controllers.di.component.ApplicationComponentProvider;

public abstract class BaseService extends Service implements ApplicationComponentProvider {
    public ApplicationComponent getApplicationComponent()
    {
        return ((MyApplication) getApplication()).getApplicationComponent();
    }
}
