package com.fewbits.postwatcher.domain.events;

public class AbstractEvent {
    private Enum type;

    protected AbstractEvent(Enum type)
    {
        this.type = type;
    }

    public Enum getType()
    {
        return this.type;
    }
}
