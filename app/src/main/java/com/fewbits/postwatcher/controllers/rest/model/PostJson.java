package com.fewbits.postwatcher.controllers.rest.model;

import com.fewbits.postwatcher.domain.Post;
import com.google.gson.annotations.SerializedName;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;

import java.util.Arrays;

@Mappable( with = Post.class )
public class PostJson
{
    @SerializedName("id")
    private String id;

    @SerializedName("url")
    private String url;

    @SerializedName("slug")
    private String slug;

    @SerializedName("tags")
    private String[] tags;

    @SerializedName("url-with-slug")
    private String urlWithSlug;

    @SerializedName("format")
    private String format;

    @SerializedName("type")
    private String type;

    @SerializedName("date-gmt")
    private String dateGmt;

    @SerializedName("date")
    private String date;

    @SerializedName("unix-timestamp")
    private long timestamp;

    @Mapped( toField = "title")
    @SerializedName("photo-caption")
    private String photoCaption;

    @Mapped( toField = "photoLowQuality")
    @SerializedName("photo-url-250")
    private String lowQualityPhotoUrl;

    @Mapped( toField = "photoGoodQuality")
    @SerializedName("photo-url-500")
    private String goodQualityPhotoUrl;

    @SerializedName("width")
    private String width;

    @SerializedName("height")
    private String height;

    @SerializedName("photos")
    private PhotoJson[] photos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public String getUrlWithSlug() {
        return urlWithSlug;
    }

    public void setUrlWithSlug(String urlWithSlug) {
        this.urlWithSlug = urlWithSlug;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDateGmt() {
        return dateGmt;
    }

    public void setDateGmt(String dateGmt) {
        this.dateGmt = dateGmt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getPhotoCaption() {
        return photoCaption;
    }

    public void setPhotoCaption(String photoCaption) {
        this.photoCaption = photoCaption;
    }

    public String getLowQualityPhotoUrl() {
        return lowQualityPhotoUrl;
    }

    public void setLowQualityPhotoUrl(String lowQualityPhotoUrl) {
        this.lowQualityPhotoUrl = lowQualityPhotoUrl;
    }

    public String getGoodQualityPhotoUrl() {
        return goodQualityPhotoUrl;
    }

    public void setGoodQualityPhotoUrl(String goodQualityPhotoUrl) {
        this.goodQualityPhotoUrl = goodQualityPhotoUrl;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public PhotoJson[] getPhotos() {
        return photos;
    }

    public void setPhotos(PhotoJson[] photos) {
        this.photos = photos;
    }

    @Override
    public String toString() {
        return "PostJson{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", slug='" + slug + '\'' +
                ", tags=" + Arrays.toString(tags) +
                ", urlWithSlug='" + urlWithSlug + '\'' +
                ", format='" + format + '\'' +
                ", type='" + type + '\'' +
                ", dateGmt='" + dateGmt + '\'' +
                ", date='" + date + '\'' +
                ", timestamp=" + timestamp +
                ", photoCaption='" + photoCaption + '\'' +
                ", lowQualityPhotoUrl='" + lowQualityPhotoUrl + '\'' +
                ", goodQualityPhotoUrl='" + goodQualityPhotoUrl + '\'' +
                ", width='" + width + '\'' +
                ", height='" + height + '\'' +
                ", photos=" + Arrays.toString(photos) +
                '}';
    }
}
