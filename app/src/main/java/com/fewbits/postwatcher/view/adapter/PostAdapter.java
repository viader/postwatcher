package com.fewbits.postwatcher.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fewbits.postwatcher.R;
import com.fewbits.postwatcher.domain.Post;
import com.fewbits.postwatcher.view.viewholder.PostViewHolder;
import com.squareup.picasso.Picasso;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostViewHolder> {

    private List<Post> posts;
    private Context context;

    public PostAdapter(Context context, List<Post> posts) {
        this.posts = posts;
        this.context = context;
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_post_cell, parent, false);
        PostViewHolder viewHolder = new PostViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onViewRecycled(PostViewHolder holder) {
        super.onViewRecycled(holder);
        holder.imageView.setImageDrawable(null);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        Post post = posts.get(position);
        if(!TextUtils.isEmpty(post.getTitle())) {
            holder.titleTextView.setHtmlFromString(post.getTitle(), new HtmlTextView.LocalImageGetter());
        }
        else {
            holder.titleTextView.setText("");
        }
        if(!TextUtils.isEmpty(post.getPhotoLowQuality())) {
            Picasso.with(context).cancelRequest(holder.imageView);
            Picasso.with(context).load(post.getPhotoLowQuality()).into(holder.imageView);
        }
        else {
            holder.imageView.setImageDrawable(null);
        }

        holder.itemView.setTag(post);
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }
}
