package com.fewbits.postwatcher.domain.events;

public class DataEvent extends AbstractEvent {

    public enum Type
    {
        NEW_POSTS
    }

    public DataEvent(Type type)
    {
        super(type);
    }
}
