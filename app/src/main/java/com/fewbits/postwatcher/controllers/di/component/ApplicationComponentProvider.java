package com.fewbits.postwatcher.controllers.di.component;

public interface ApplicationComponentProvider {
    ApplicationComponent getApplicationComponent();
}
