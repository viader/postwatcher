package com.fewbits.postwatcher.controllers.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TumblelogJson
{
    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("name")
    private String name;

    @SerializedName("timezone")
    private String timezone;

    @SerializedName("cname")
    private String cname;

    @SerializedName("feeds")
    private String[] feeds;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String[] getFeeds() {
        return feeds;
    }

    public void setFeeds(String[] feeds) {
        this.feeds = feeds;
    }

    @Override
    public String toString() {
        return "TumblelogJson{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", timezone='" + timezone + '\'' +
                ", cname='" + cname + '\'' +
                ", feeds=" + Arrays.toString(feeds) +
                '}';
    }
}
