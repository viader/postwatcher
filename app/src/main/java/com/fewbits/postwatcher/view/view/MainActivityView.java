package com.fewbits.postwatcher.view.view;

import com.fewbits.postwatcher.domain.Post;

import java.util.List;

public interface MainActivityView {

    public void setItems(List<Post> items);
}
