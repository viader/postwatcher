package com.fewbits.postwatcher.view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.fewbits.postwatcher.R;

import org.sufficientlysecure.htmltextview.HtmlTextView;

public class PostViewHolder extends RecyclerView.ViewHolder {

    public HtmlTextView titleTextView;
    public ImageView imageView;

    public PostViewHolder(View itemView) {
        super(itemView);
        titleTextView = (HtmlTextView) itemView.findViewById(R.id.adapter_post_cell_title);
        imageView = (ImageView) itemView.findViewById(R.id.adapter_post_cell_image);
    }

}
