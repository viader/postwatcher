package com.fewbits.postwatcher.controllers.rest;

public class ConnectivityManagerMock implements ConnectivityManager
{
    @Override
    public boolean isConnectionAvailable()
    {
        return true;
    }
}
