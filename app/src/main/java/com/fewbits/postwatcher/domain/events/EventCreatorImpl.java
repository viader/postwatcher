package com.fewbits.postwatcher.domain.events;

public class EventCreatorImpl implements EventCreator {

    private EventBus bus;

    public EventCreatorImpl(EventBus bus) {
        this.bus = bus;
    }

    @Override
    public void createNewPostsEvent() {
        bus.postEvent(new DataEvent(DataEvent.Type.NEW_POSTS));
    }

    @Override
    public void createRequestFailEvent() {
        bus.postEvent(new RestEvent(RestEvent.Type.FAIL));
    }
}
