package com.fewbits.postwatcher.controllers.rest.endpoint;

public class TumblrEndpointMock implements DynamicEndpoint {

    @Override
    public void setObject(Object object) {

    }

    @Override
    public String getUrl() {
        return "http://localhost:1111/";
    }

    @Override
    public String getName() {
        return "tumblr";
    }
}
