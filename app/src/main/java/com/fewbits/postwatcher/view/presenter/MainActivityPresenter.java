package com.fewbits.postwatcher.view.presenter;

public interface MainActivityPresenter {
    void onResume();
    void onPause();
    boolean onQueryTextSubmit(String query);
    boolean onQueryTextChange(String query);
}
