package com.fewbits.postwatcher.controllers.rest;

import com.fewbits.postwatcher.BuildConfig;
import com.fewbits.postwatcher.controllers.ApplicationMock;
import com.fewbits.postwatcher.controllers.di.module.ApplicationModuleMock;
import com.fewbits.postwatcher.domain.DataManager;
import com.fewbits.postwatcher.domain.events.DataEvent;
import com.fewbits.postwatcher.domain.events.EventBus;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.squareup.otto.Subscribe;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLooper;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE,
        constants = BuildConfig.class,
        application = ApplicationMock.class)
public class RetrofitAdapterIT {
    protected ApplicationMock application;
    protected RestManager restManager;
    protected DataManager dataManager;
    protected DataEvent dataEvent;

    protected EventBus bus;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(1111);

    @Before
    public void setup()
    {
        application = (ApplicationMock) RuntimeEnvironment.application;
        ApplicationModuleMock module = new ApplicationModuleMock(application);
        application.setApplicationModule(module);

        restManager = application.getApplicationComponent().getRestManager();
        dataManager = application.getApplicationComponent().getDataManager();
        bus = application.getApplicationComponent().getEventBus();
        bus.register(this);
    }

    @Test
    public void shouldGetGoodResponse()
    {
        stubFor(get(urlMatching("/api/read.*"))
                .atPriority(5)
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBodyFile("posts.json")));
        dataEvent = null;
        restManager.getPosts(0, 10);
        while(dataEvent==null) {
            ShadowLooper.runUiThreadTasks();
        }
        assertThat(dataManager.getPostRepository().getAllPosts().size()).isEqualTo(20);
    }

    @Subscribe
    public void handleDataEvent(DataEvent event)
    {
        dataEvent = event;
    }

}
