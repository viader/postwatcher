package com.fewbits.postwatcher.domain.events;

public interface EventCreator {

    void createNewPostsEvent();
    void createRequestFailEvent();

}
