package com.fewbits.postwatcher.view.presenter;

import android.support.annotation.NonNull;

import com.fewbits.postwatcher.controllers.di.component.ApplicationComponent;
import com.fewbits.postwatcher.controllers.rest.RestManager;
import com.fewbits.postwatcher.domain.DataManager;
import com.fewbits.postwatcher.domain.Post;
import com.fewbits.postwatcher.domain.events.DataEvent;
import com.fewbits.postwatcher.domain.events.EventBus;
import com.fewbits.postwatcher.view.view.MainActivityView;
import com.squareup.otto.Subscribe;

import java.util.List;

import javax.inject.Inject;

public class MainActivityPresenterImpl implements MainActivityPresenter {

    @Inject
    DataManager dataManager;
    @Inject
    RestManager restManager;
    @Inject
    EventBus bus;

    protected MainActivityView view;
    protected List<Post> posts;

    public MainActivityPresenterImpl(@NonNull MainActivityView view, @NonNull ApplicationComponent applicationComponent) {
        this.view = view;
        applicationComponent.inject(this);
        restManager.getPosts(0,50);
    }

    @Override
    public void onResume() {
        posts = dataManager.getPostRepository().getAllPosts();
        setItems();
        bus.register(this);
    }

    @Override
    public void onPause() {
        bus.unregister(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        restManager.rebuildAdapter(query);
        dataManager.getPostRepository().clearPosts();
        restManager.getPosts(0,50);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        return true;
    }

    @Subscribe
    public void handleDataEvent(DataEvent event)
    {
        if(event.getType()==DataEvent.Type.NEW_POSTS) {
            view.setItems(dataManager.getPostRepository().getAllPosts());
        }
    }

    private void setItems() {
        view.setItems(posts);
    }

}