package com.fewbits.postwatcher.controllers.di.module;

import android.content.Context;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fewbits.postwatcher.controllers.MyApplication;
import com.fewbits.postwatcher.controllers.rest.ConnectivityManager;
import com.fewbits.postwatcher.controllers.rest.ConnectivityManagerImpl;
import com.fewbits.postwatcher.controllers.rest.RestManager;
import com.fewbits.postwatcher.controllers.rest.RestManagerImpl;
import com.fewbits.postwatcher.controllers.rest.endpoint.DynamicEndpoint;
import com.fewbits.postwatcher.controllers.rest.endpoint.TumblrEndpoint;
import com.fewbits.postwatcher.domain.DataManager;
import com.fewbits.postwatcher.domain.DataManagerImpl;
import com.fewbits.postwatcher.domain.events.EventBus;
import com.fewbits.postwatcher.domain.events.EventBusImpl;
import com.fewbits.postwatcher.domain.events.EventCreator;
import com.fewbits.postwatcher.domain.events.EventCreatorImpl;
import com.fewbits.postwatcher.domain.repository.PostRepository;
import com.fewbits.postwatcher.domain.repository.PostRepositoryImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    protected final MyApplication application;

    public ApplicationModule(MyApplication application)
    {
        this.application = application;
    }

    @Provides
    @Singleton
    protected MyApplication provideMyApplication()
    {
        return application;
    }


    @Provides
    @Singleton
    protected DataManager providesDataManager(PostRepository postRepository)
    {
        DataManagerImpl dataManager = new DataManagerImpl();
        dataManager.setPostRepository(postRepository);
        return dataManager;
    }

    @Provides
    @Singleton
    protected PostRepository providesPostRepository(EventCreator eventCreator)
    {
        return new PostRepositoryImpl(eventCreator);
    }

    @Provides
    @Singleton
    protected RestManager providesRestManager(DynamicEndpoint endpoint, ConnectivityManager connectivityManager, DataManager dataManager, EventCreator eventCreator, Gson gson)
    {
        return new RestManagerImpl(endpoint, connectivityManager, dataManager, eventCreator, gson);
    }

    @Provides
    @Singleton
    protected EventCreator providesEventCreator(EventBus eventBus)
    {
        return new EventCreatorImpl(eventBus);
    }

    @Provides
    @Singleton
    protected EventBus providesEventBus()
    {
        return new EventBusImpl();
    }


    @Provides
    @Singleton
    protected DynamicEndpoint providesDynamicEndpoint()
    {
        return new TumblrEndpoint("thepigeongazette");
    }

    @Provides
    @Singleton
    protected ConnectivityManager providesConnectivityManager()
    {
        return new ConnectivityManagerImpl((android.net.ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE));
    }

    @Provides
    @Singleton
    protected ObjectMapper providesObjectMapper()
    {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    @Provides
    @Singleton
    protected Gson providesGson()
    {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
    }

}
